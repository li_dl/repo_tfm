import os
import mujoco_py
import numpy as np
from gym.utils import seeding
from timeit import default_timer as timer
DEFAULT_SIZE = 64
class IRB120Env():
    def __init__(self, width, height, frame_skip, rewarding_distance, 
                                control_magnitude, reward_continuous):
        self.frame_skip = frame_skip
        self.width = width
        self.height = height
        self.viewer = None
        self._viewers = {}
        # Instantiate Mujoco model
        model_path = "irb120.xml"
        fullpath = os.path.join(
            os.path.dirname(__file__), "assets", model_path)
        if not os.path.exists(fullpath):
            raise IOError("File %s does not exist" % fullpath)
        model = mujoco_py.load_model_from_path(fullpath)
        self.sim = mujoco_py.MjSim(model)

        self.init_state = self.sim.get_state()
        self.init_qpos = self.sim.data.qpos.ravel().copy()
        self.init_qvel = self.sim.data.qvel.ravel().copy()

        # Setup actuators
        self.actuator_bounds = self.sim.model.actuator_ctrlrange
        self.actuator_low = self.actuator_bounds[:, 0]
        self.actuator_high = self.actuator_bounds[:, 1]
        self.actuator_ctrlrange = self.actuator_high - self.actuator_low
        self.num_actuators = len(self.actuator_low)

       
        # init model_data_ctrl
        
        self.sim.data.ctrl[:] = np.zeros(self.num_actuators)

        self.seed()
        self.reward_continuous = reward_continuous
        self.sum_reward = 0
        self.rewarding_distance = rewarding_distance
        self.max_threshold = 0.3

        # Target position bounds
        self.target_bounds = np.array(((0.4, 0.6), (0., 0.2), (0.02, 0.02)))
        self.target_reset_distance = 0.2

        # Setup discrete action space
        self.control_values = self.actuator_ctrlrange * control_magnitude

        self.num_actions = 5
        self.action_space = [list(range(self.num_actions))
                             ] * self.num_actuators
        self.observation_space = ((0, ), (height, width, 3))

        
        self.reset()

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _set_qpos_qvel(self, qpos, qvel):
        assert qpos.shape == (self.sim.model.nq, ) and qvel.shape == (
            self.sim.model.nv, )
        self.sim.data.qpos[:] = qpos
        self.sim.data.qvel[:] = qvel
        self.sim.forward()

    def reset(self):
        qpos = self.init_qpos
        
        # qpos[:1] = np.round(np.random.uniform(-1, 1, 1), 2)
        # qpos[1:2] = np.round(np.random.uniform(-0.3, 0.6, 1), 2)
        qvel = self.init_qvel

        # random object position start of episode
        self._reset_target()

        # set initial joint positions and velocities
        self._set_qpos_qvel(qpos, qvel)

        return self._get_obs()

    def _reset_target(self):
        # Randomize goal position within specified bounds
        self.goal = np.random.rand(3) * (self.target_bounds[:, 1] -
                                         self.target_bounds[:, 0]
                                         ) + self.target_bounds[:, 0]
        geom_positions = self.sim.model.geom_pos.copy()
        
        prev_goal_location = geom_positions[1]

        while (np.linalg.norm(prev_goal_location - self.goal) <
               self.target_reset_distance):
            self.goal = np.random.rand(3) * (self.target_bounds[:, 1] -
                                             self.target_bounds[:, 0]
                                             ) + self.target_bounds[:, 0]

        geom_positions[1] = self.goal

        self.sim.model.geom_pos[:] = geom_positions
        
    def render(self,
               mode=None,
               width=DEFAULT_SIZE,
               height=DEFAULT_SIZE,
               camera_id=None,
               camera_name=None):
        if mode == 'rgb_array':
            if camera_id is not None and camera_name is not None:
                raise ValueError("Both `camera_id` and `camera_name` cannot be"
                                 " specified at the same time.")

            no_camera_specified = camera_name is None and camera_id is None
            if no_camera_specified:
                camera_name = 'track'

            if camera_id is None and camera_name in self.model._camera_name2id:
                camera_id = self.model.camera_name2id(camera_name)

            self._get_viewer(mode).render(width, height, camera_id=1)
            # window size used for old mujoco-py:
            data = self._get_viewer(mode).read_pixels(width, height, depth=False)
            # original image is upside-down, so flip it
            return data[::-1, :, :]
            # return data
        elif mode == 'depth_array':
            self._get_viewer(mode).render(width, height)
            # window size used for old mujoco-py:
            # Extract depth part of the read_pixels() tuple
            data = self._get_viewer(mode).read_pixels(width, height, depth=True)[1]
            # original image is upside-down, so flip it
            return data[::-1, :]
        elif mode == 'human':
            self._get_viewer(mode).render()

    def _get_viewer(self, mode):
        self.viewer = self._viewers.get(mode)
        if self.viewer is None:

            if mode == 'human':        
                self.viewer = mujoco_py.MjViewer(self.sim)
            elif mode == 'rgb_array' or mode == 'depth_array':
                self.viewer = mujoco_py.MjRenderContextOffscreen(self.sim, -1, opengl_backend='glfw')
            
            self._viewer_setup()
            self._viewers[mode] = self.viewer
        return self.viewer

    def _viewer_setup(self):
        body_id = self.sim.model.body_name2id('robot0:base_link')
        lookat = self.sim.data.body_xpos[body_id]
        for idx, value in enumerate(lookat):
            self.viewer.cam.lookat[idx] = value
        self.viewer.cam.distance = 2
        self.viewer.cam.azimuth = 180
        self.viewer.cam.elevation = -30
        

    def _get_obs_joint(self):   
        return np.concatenate(
            [self.sim.data.qpos.flat[:], self.sim.data.qvel.flat[:]])

    def _get_obs_rgb_view1(self):
        self._get_viewer('rgb_array').render(self.width, self.height, camera_id=-1)
        # start = timer()
        obs_rgb_view1 = self._get_viewer('rgb_array').read_pixels(self.width, self.height, depth=False)
        # end = timer()
        # print(f"Get_image: {end-start}" )
        return obs_rgb_view1[::-1, :]

    # def _get_obs_rgb_view2(self):
    #     self._get_viewer('rgb_array').render(self.width, self.height, camera_id=1)
    #     obs_rgb_view2 = self._get_viewer('rgb_array').read_pixels(self.width, self.height, depth=False)
    #     return obs_rgb_view2[::-1, :]

    def _get_obs(self):
        return (self._get_obs_joint(), self._get_obs_rgb_view1())

    def _do_simulation(self, ctrl):
        '''Do one step of simulation, taking new control as target

        Arguments:
            ctrl {np.array(num_actuator)}  -- new control to send to actuators
        '''
        # ctrl = np.min((ctrl, self.actuator_high), axis=0)
        # ctrl = np.max((ctrl, self.actuator_low), axis=0)
        
        self.sim.data.ctrl[:] = ctrl

        for _ in range(self.frame_skip):
            self.sim.step()

    # @profile(immediate=True)
    def step(self, a):
        # dist = np.zeros(2)
        done = False
        new_control = np.copy(self.sim.data.ctrl).flatten()
        # reward = 0
        # Compute reward:
        # dist[0] = np.linalg.norm(
        #     self.sim.data.get_body_xpos("robot0:l_gripper_finger_link") - self.goal)
        # dist[1] = np.linalg.norm(
        #     self.sim.data.get_body_xpos("robot0:r_gripper_finger_link") - self.goal)
        
        dist = np.linalg.norm(
            self.sim.data.get_site_xpos("robot0:grip") - self.goal)                
        # print (dist)
        #####continuous or sparse positive reward
        # if self.reward_continuous:
        #     reward = (((dist)+0.01)**-1)*0.1
        # else:

        #     if dist < self.rewarding_distance:
        #         reward = 1
        #     else:
        #         reward = 0

        
        
        ###### Continuous and sparse positive + negative reward
        # if self.reward_continuous:

        #     if dist > 2*self.rewarding_distance:
        #         reward = -(2*dist)**2
        #     else:
        #         reward = (((dist)+0.01)**-1)*0.1
        # else:

        #     if dist < self.rewarding_distance:
        #         reward = 1
        #     else:
        #         reward = -1

        ##### Continuous and sparse negative reward 
        if self.reward_continuous:
            reward = -(2*dist)**2
        else:
            if dist > self.rewarding_distance:
                reward = -1
            else:
                reward = 0
        # print(dist)
        # if dist < 0.01:
        #     done = True
        # Transform discrete actions to continuous controls
        for i in range(self.num_actuators):
            '''
            0 = 0 velocity
            1 = small positive velocity
            2 = large positive velocity
            3 = small negative velocity
            4 = large negative velocity
            '''
            if a[i] == 0:
                new_control[i] = 0
            if a[i] == 1:
                new_control[i] = self.control_values[i] / 2
            if a[i] == 2:
                new_control[i] = self.control_values[i]
            if a[i] == 3:
                new_control[i] = -self.control_values[i] / 2
            elif a[i] == 4:
                new_control[i] = -self.control_values[i]

      
        # Do one step of simulation
        self._do_simulation(new_control)
        self.sum_reward  += reward
        # print(reward)
        
        return self._get_obs(), reward, done
