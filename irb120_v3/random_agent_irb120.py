import argparse
import numpy as np
from irb120 import IRB120Env
import mujoco_py
import time
from timeit import default_timer as timer
import matplotlib.pyplot as plt
import cv2
plt.ion()

parser = argparse.ArgumentParser(description='A3C')
parser.add_argument('--width', type=int, default=1028, help='RGB width')
parser.add_argument('--height', type=int, default=1028, help='RGB height')
parser.add_argument('--frame_skip', type=int, default=100, help="Frame skipping in environment. Repeats last agent action.")
parser.add_argument('--rewarding_distance', type=float, default=0.05, help='Distance from target at which reward is provided.')
parser.add_argument('--control_magnitude', type=float, default=0.8, help='Fraction of actuator range used as control inputs.')
parser.add_argument('--reward_continuous', action='store_true', help='if True, provides rewards at every timestep')
parser.add_argument('--render', action='store_true', help='if True, sets up MuJoCo Viewer instead of Matplotlib')


class IRB120Random():
    def __init__(self, width, height, frame_skip, rewarding_distance, control_magnitude,
                 reward_continuous, render):
        self.env = IRB120Env(width, height, frame_skip, rewarding_distance,
                           control_magnitude, reward_continuous)
        self.render = render

    def run(self):
        
        if self.render:
            (_, obs_rgb_view1) = self.env.reset()
            obs_rgb = cv2.resize(obs_rgb_view1, (64,64))
            f, ax = plt.subplots()
            im = ax.imshow(obs_rgb)

        while True:
            self.env.reset()

            while True:

                # random action selection
                action = np.array([4, 0, 0 ,0 ,0 ,0])
                # action = np.random.choice([0, 1, 2, 3, 4], 6)

                # take the random action and observe the reward and next state (2 rgb views and proprioception)
                # start = timer()
                (obs_joint, obs_rgb_view1), reward, done = self.env.step(action)
                obs_rgb = cv2.resize(obs_rgb_view1, (64,64))
                # end = timer()
                # print(f"Get_image: {end-start}" )
                # print("action : ", action)
                # print("reward : ", reward)

                if done:
                    break

                if self.render:
                    im.set_data(obs_rgb)
                    plt.draw()
                    plt.pause(0.01)


if __name__ == '__main__':
    args = parser.parse_args()
    print(' ' * 26 + 'Options')
    for k, v in vars(args).items():
        print(' ' * 26 + k + ': ' + str(v))

    agent = IRB120Random(args.width, args.height, args.frame_skip,
                               args.rewarding_distance, args.control_magnitude,
                               args.reward_continuous, args.render)
    agent.run()
