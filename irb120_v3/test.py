# -*- coding: utf-8 -*-
import os
import time
from datetime import datetime
import torch
from torch.autograd import Variable

from irb120 import IRB120Env
from model import ActorCritic
from utils import state_to_tensor, plot_line
import cv2
import matplotlib.pyplot as plt


def test(rank, args, T, shared_model):
    torch.manual_seed(args.seed + rank)

    if args.fine_render:
    	args.height = 640
    	args.width = 640


    env = IRB120Env(args.width,
                  args.height,
                  args.frame_skip,
                  args.rewarding_distance,
                  args.control_magnitude,
                  args.reward_continuous)
    env.seed(args.seed + rank)

    if args.fine_render:

	    (_, obs_rgb) = env.reset()
	    # obs_rgb = cv2.resize(obs_rgb, (64,64))
	    plt.ion()
	    f, ax = plt.subplots()
	    im = ax.imshow(obs_rgb)


    if args.render:
        (_, obs_rgb_view2) = env.reset()
        plt.ion()
        f, ax = plt.subplots()
        im = ax.imshow(obs_rgb_view2)



    model = ActorCritic(None, args.non_rgb_state_size, None, args.hidden_size)
    model.eval()
    can_test = True  # Test flag
    t_start = 1  # Test step counter to check against global counter
    rewards, steps = [], []  # Rewards and steps for plotting
    n_digits = str(
        len(str(args.T_max)))  # Max num. of digits for logging steps
    done = True  # Start new episode
    success = 0
    while T.value() <= args.T_max - 1:
        if can_test:
            t_start = T.value()  # Reset counter

            # Evaluate over several episodes and average results
            avg_rewards, avg_episode_lengths = [], []
            for _ in range(args.evaluation_episodes):
                while True:
                    # Reset or pass on hidden state
                    if done:
                        # Sync with shared model every episode
                        model.load_state_dict(shared_model.state_dict())
                        with torch.no_grad():
                            hx = torch.zeros(1, args.hidden_size)
                            cx = torch.zeros(1, args.hidden_size)
                        # Reset environment and done flag
                        if args.fine_render:
                        	obs, obs_rgb = env.reset()
                        	state = state_to_tensor((obs, cv2.resize(obs_rgb, (64,64))))

                        else:
	                        state = state_to_tensor(env.reset())
                        action, reward, done, episode_length = (0, 0, 0, 0, 0,
                                                                0), 0, False, 0
                        reward_sum = 0
                    # print(state[1].shape)
                    # Calculate policy
                    with torch.no_grad():
                        policy, _, (hx, cx) = model(state[0], state[1], (hx.detach(), cx.detach()))  # Break graph for memory efficiency
                    # for p in policy:
                    #     print(p.max(1)[1].data[0])
                    # Choose action greedily

                    action = [p.max(1)[1].data[0] for p in policy]

                    # Step
                    if args.fine_render:
                    	state, reward, done = env.step(action)
                    	obs_rgb = state[1]
                    	state = state_to_tensor((state[0],cv2.resize(obs_rgb, (64,64)))) 
                    else:

	                    state, reward, done = env.step(action)
	                    obs_rgb_view2 = state[1]
	                    state = state_to_tensor(state)

	                    
                    reward_sum += reward
                    done = done or episode_length >= args.max_episode_length - 1 # Stop episodes at a max length
                    episode_length += 1  # Increase episode counter
                    # print(reward_sum, episode_length)
                    # Optionally render validation states
                    if args.render:
                        
                        im.set_data(obs_rgb_view2)
                        plt.draw()
                        plt.pause(0.001)

                    if args.fine_render:

                    	im.set_data(obs_rgb)

                    	plt.draw()

                    	plt.pause(0.001)

                    # Log and reset statistics at the end of every episode
                    if done:
                    	# if episode_length < args.max_episode_length - 1:
                    	# 	success +=1
                        avg_rewards.append(reward_sum)
                        avg_episode_lengths.append(episode_length)
                        break

            print(('[{}] Step: {:<' + n_digits +
                   '} Avg. Reward: {:<8} Avg. Episode Length: {:<8}').format(
                       datetime.utcnow().strftime(
                           '%Y-%m-%d %H:%M:%S,%f')[:-3], t_start,
                       sum(avg_rewards) / args.evaluation_episodes,
                       sum(avg_episode_lengths) / args.evaluation_episodes))

            rewards.append(avg_rewards)  # Keep all evaluations
            steps.append(t_start)
            plot_line(steps, rewards)  # Plot rewards
            torch.save(model.state_dict(),
                       os.path.join('results_4', str(t_start) +
                                    '_model.pth'))  # Checkpoint model params
            can_test = False  # Finish testing
            if args.evaluate:
            	# print(success)
                return
        else:
            if T.value() - t_start >= args.evaluation_interval:
                can_test = True

        time.sleep(0.001)  # Check if available to test every millisecond
