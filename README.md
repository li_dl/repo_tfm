# README #

### How do I get set up? ###

**python=3.6

##### Download gym
git clone https://github.com/openai/gym.git

cd gym

pip install -e .

include env/robotics and env/_init_.py from this repo to your gym library


##### Download baselines
git clone https://github.com/openai/baselines.git

cd baselines

pip install tensorflow==1.14

pip install -e .

##### Download mujoco-py. v 2.0.2.9
git clone https://github.com/openai/mujoco-py.git

cd mujoco-py

python setup.py install

Check that mujoco-py can be imported correctly 

##### Running model example
python -m baselines.run --alg=her --env=FetchReach-v2 --num_timesteps=5000 --play 

python -m baselines.run --alg=her --env=FetchReach-v2 --num_timesteps=5000 --save_path=/your/path/ --play

python -m baselines.run --alg=her --env=FetchReach-v2 --num_timesteps=5000 --load_path=/your/trained_model/path/ --play

